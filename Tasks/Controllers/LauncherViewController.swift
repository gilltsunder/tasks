//
//  LauncherViewController.swift
//  Tasks
//
//  Created by Влад Третьяк on 4/16/19.
//  Copyright © 2019 Влад Третьяк. All rights reserved.
//

import UIKit
import Firebase

class LauncherViewController: UIViewController {

    @IBOutlet weak var userEmail: UITextField!
    @IBOutlet weak var userPassword: UITextField!
    @IBOutlet weak var logginBtn: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()

    }
    
    @IBAction func login(_ sender: Any) {
        Auth.auth().signIn(withEmail: userEmail.text!, password: userPassword.text!) { (authResult, error) in
            if error != nil {
            } else {
                print("yep!")
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let VC = storyboard.instantiateViewController(withIdentifier: "Main")
                self.present(VC, animated: true, completion: nil)
            }
            
        }
    }
    

}
