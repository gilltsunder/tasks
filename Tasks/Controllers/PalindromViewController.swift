//
//  PalindromViewController.swift
//  Tasks
//
//  Created by Влад Третьяк on 4/19/19.
//  Copyright © 2019 Влад Третьяк. All rights reserved.
//

import UIKit

class PalindromViewController: UIViewController {

    @IBOutlet weak var textfield: UITextField!
    @IBOutlet weak var resultLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()


    }
    
    func Palindrome(_ text:String) -> Bool {
        var textArr = Array (text)
        
        for (index, value) in (0..<textArr.count).reversed().enumerated() {
            if textArr[index] != textArr[value] {
                resultLabel.text = "false"
                return false
            }
        }
        resultLabel.text = "true"
        return true
    }
    
    @IBAction func palindromBtm(_ sender: Any) {
        
        let test = textfield.text
        
        Palindrome(test!)
        
        
    }
    
}
