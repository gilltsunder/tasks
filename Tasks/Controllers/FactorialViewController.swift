//
//  FactorialViewController.swift
//  Tasks
//
//  Created by Влад Третьяк on 4/17/19.
//  Copyright © 2019 Влад Третьяк. All rights reserved.
//

import UIKit

class FactorialViewController: UIViewController {

    @IBOutlet weak var numberTextField: UITextField!
    @IBOutlet weak var resultLabel: UILabel!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()


    }
    
    func factorial(_ n: Int) -> Int {
        if n == 0 {
            return 1
        }
        var a = 1
        for i in 1...n {
            a *= Int(i)
        }
        let myString = String(a)
        let array = myString.compactMap{Int(String($0))}
        var t = 0
        for i in 1...array.count {
            t += Int(i)
        }
        print(t)
        resultLabel.text = String(t)
        return t
        
    }
 
    
    
    
    @IBAction func resultBtn(_ sender: Any) {
        
        let a:Int? = Int(numberTextField.text!)
        
        
        factorial(a!)
        
    
        
        
       // factorial(numberTextField.text)
        
    }
    
}
